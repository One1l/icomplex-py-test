from django.db.models.signals import pre_save
from django.db.models.signals import post_save
from django.dispatch import receiver
from models import Category


@receiver(pre_save)
def pre_category_save_receiver(sender, instance, **kwargs):
    """Refresh category URL
    """
    if type(instance) == Category:
        instance.refresh_url()


@receiver(post_save)
def post_category_save_receiver(sender, instance, **kwargs):
    """Refresh category's subcategory URLs
    """
    if not type(instance) == Category:
        return
    subcategories = instance.category_set.all()
    for subcategory in subcategories:
        subcategory.save()
