# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0006_auto_20150814_1136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(default=b'', max_length=128, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='url',
            field=models.CharField(default=b'', max_length=512, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='url_name',
            field=models.CharField(default=b'', max_length=128, null=True, blank=True),
        ),
    ]
