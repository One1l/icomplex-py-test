# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0004_auto_20150814_1123'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='url',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='category',
            name='url_name',
            field=models.CharField(default=b'', max_length=128),
        ),
    ]
