# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.AddField(
            model_name='category',
            name='name',
            field=models.CharField(default=b'', max_length=128),
        ),
        migrations.AddField(
            model_name='item',
            name='category',
            field=models.ForeignKey(to='catalog.Category'),
        ),
    ]
