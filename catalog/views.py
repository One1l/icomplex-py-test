# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import View
from django.core.paginator import Paginator
from models import Category
from models import Item
from forms import SearchForm


class Basic(View):
    def generate_category_chain(self, category_url):
        category_url_parts = []
        if category_url:
            category_url_parts = category_url.split('/')
        category_chain = []
        while category_url_parts:
            category_url = "/".join(category_url_parts)
            categories = Category.objects.filter(url=category_url)
            if not categories:
                return []
            category = categories[0]
            category_chain.insert(0, {'name': category.name, 'url': category_url})
            category_url_parts.pop()
        category_chain.insert(0, {'name': u'Каталог', 'url': ''})
        return category_chain

    def get_category_items(self, category_url):
        categories = Category.objects.filter(url=category_url)
        if not categories:
            return []
        category = categories[0]
        items = []
        category_image = category.image
        if category.item_set.exists():
            category_items = category.item_set.all()
            for category_item in category_items:
                if not category_item.image:
                    category_item.image = category_image
            items += category.item_set.all()
        subcategories = []
        if category.category_set.exists():
            subcategories = category.category_set.all()
        subcategory_urls = [subcategory.url for subcategory in subcategories]
        for subcategory_url in subcategory_urls:
            items += self.get_category_items(subcategory_url)
        return items

    def normalize_url(self, url):
        url_parts = url.split('/')
        url_parts = [url_part for url_part in url_parts if url_part]
        return "/".join(url_parts)

    def get_subcategories(self, category_url):
        return Category.objects.get(url=category_url).category_set.all()


class Catalog(Basic):
    def get(self, request, category_url, page_index):
        if not page_index:
            page_index = request.GET.get('page', 1)
        category_url = self.normalize_url(category_url)
        if not Category.objects.filter(url=category_url):
            category_url = ''
        items = self.get_category_items(category_url)
        paginator = Paginator(items, 12)
        page = paginator.page(page_index)
        category_chain = self.generate_category_chain(category_url)
        subcategories = self.get_subcategories(category_url)
        search_form = SearchForm()
        return render(request,
                      'catalog/catalog.html',
                      {'category_chain': category_chain,
                       'category_url': category_url,
                       'subcategories': subcategories,
                       'page': page,
                       'search_form': search_form})


class Search(Basic):
    def get(self, request, page_index):
        if not page_index:
            page_index = request.GET.get('page', 1)
        search_form = SearchForm(request.GET)
        items = []
        if search_form.is_valid():
            items = Item.objects.filter(name__contains=search_form.cleaned_data['text'])
        paginator = Paginator(items, 12)
        page = paginator.page(page_index)
        category_chain = self.generate_category_chain('')
        subcategories = self.get_subcategories('')
        search_form = SearchForm()
        return render(request,
                      'catalog/search.html',
                      {'category_chain': category_chain,
                       'subcategories': subcategories,
                       'page': page,
                       'search_form': search_form})


class ItemView(Basic):
    def get(self, request, id):
        category_chain = self.generate_category_chain('')
        subcategories = self.get_subcategories('')
        search_form = SearchForm()
        item = None
        if id:
            items = Item.objects.filter(id=id)
            if items:
                item = items[0]
        return render(request,
                      'catalog/item.html',
                      {'category_chain': category_chain,
                       'subcategories': subcategories,
                       'search_form': search_form,
                       'item': item})
