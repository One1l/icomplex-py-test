from django.db import models


class Category(models.Model):
    parent = models.ForeignKey("Category", blank=True, null=True)
    name = models.CharField(max_length=128)
    url_name = models.CharField(max_length=128)
    url = models.CharField(max_length=512, default='', blank=True, null=True)
    image = models.ImageField(default='', blank=True, null=True)

    def __str__(self):
        return self.name

    def refresh_url(self):
        current_node = self
        url_parties = []
        while current_node.parent:
            print current_node.url_name
            url_parties.insert(0, current_node.url_name)
            current_node = current_node.parent
        self.url = "/".join(url_parties)


class Item(models.Model):
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=512)
    image = models.ImageField(default='', blank=True, null=True)

    def __str__(self):
        return self.name

from receivers import pre_category_save_receiver
