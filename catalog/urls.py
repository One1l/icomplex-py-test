from django.conf.urls import url
import views

urlpatterns = [
    url(r'^search/?(?P<page_index>\d*)$', views.Search.as_view(), name='search'),
    url(r'^item/?(?P<id>\d*)$', views.ItemView.as_view(), name='item'),
    url(r'^(?P<category_url>[^\d]*)/?(?P<page_index>\d*)$', views.Catalog.as_view(), name='catalog'),
    url(r'^(?P<category_url>.*)$', views.Catalog.as_view(), name='catalog_get_params'),
]
